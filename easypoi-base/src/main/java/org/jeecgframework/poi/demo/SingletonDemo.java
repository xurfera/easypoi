package org.jeecgframework.poi.demo;

/**
 * Created by xugenfa on 16/6/4.
 */
public class SingletonDemo {

    public static void main(String[] args) {
        Singleton instance = Singleton.getInstance();
        System.out.println(instance);
    }
}
