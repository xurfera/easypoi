package org.jeecgframework.poi.demo;

/**
 * Created by xugenfa on 16/6/4.
 */
public class Singleton {

    private Singleton () {

    }

    private static class Holder {
        static Singleton instance = new Singleton();
    }

    public static Singleton getInstance(){
        return Holder.instance;
    }
}
